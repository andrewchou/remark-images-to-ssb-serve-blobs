# remark-images-to-ssb-serve-blobs

Processes markdown that has images elements referring to SSB blob images and links them to the correct address according to the sbot plugin `ssb-serve-blobs` where the image is being hosted.
root level.

Note on versioning: this package's version equals the version of `ssb-serve-blobs` which it is compatible with, appended with a counter for further versioning. E.g. `remark-images-to-ssb-serve-blobs@2.0.0-3` would be the *third version* of this package which is compatible with `ssb-serve-blobs@2.0.0`.

## Installation

```sh
npm install remark-images-to-ssb-serve-blobs
```

## Usage

```js
// ...
var imagesToSsbServeBlobs = require('remark-images-to-ssb-serve-blobs');

unified()
  .use(parse)
   // Optional `port` option can be passed. 26835 is the default used by ssb-serve-blobs
  .use(imagesToSsbServeBlobs, {port: 26835})
// ...
```

Markdown document:

```
# Title

Take a look at this scenery:

![scenery](&Pe5kTo/V/w4MToasp1IuyMrMcCkQwDOdyzbyD5fy4ac=.sha256)
```

Input AST:

```
root[3] (1:1-7:1, 0-105)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
├─ paragraph[1] (4:1-4:29, 10-38)
│  └─ text: "Take a look at this scenery:" (4:1-4:29, 10-38)
└─ paragraph[1] (6:1-6:65, 40-104)
   └─ image (6:1-6:65, 40-104) [url="&Pe5kTo/V/w4MToasp1IuyMrMcCkQwDOdyzbyD5fy4ac=.sha256"][alt="scenery"]
```

Output AST:

```
root[3] (1:1-7:1, 0-105)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
├─ paragraph[1] (4:1-4:29, 10-38)
│  └─ text: "Take a look at this scenery:" (4:1-4:29, 10-38)
└─ paragraph[1] (6:1-6:65, 40-104)
   └─ image (6:1-6:65, 40-104) [url="http://localhost:26835/%26Pe5kTo%2FV%2Fw4MToasp1IuyMrMcCkQwDOdyzbyD5fy4ac%3D.sha256"][alt="scenery"]
```

## License

[MIT](LICENSE)